> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS4381 - Mobile Web Application Development 

## Nicholas Buddemeyer

### Assignment 4 Requirements:

*Three Parts:*

1. Review subdirectories and files
2. Open index.php and review code
3. Chapter Questions (Chs 9,10,19)


#### README.md file should include the following items:

* Screenshots of site

 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


####    Assignment Screenshots:

#### 1.Screenshot of Petstore :

![My Portfolio](img/a4.png)


#### 2.Screenshot of Petstore :
![My Portfolio](img/a4_2.png)


#### 3.Screenshot of Petstore:
![My Portfolio](img/a4_3.png)



