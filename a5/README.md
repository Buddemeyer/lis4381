> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS4381 - Mobile Web Application Development 

## Nicholas Buddemeyer

### Assignment 5 Requirements:

*Four Parts:*

1. Review subdirectories and files
2. Open index.php and review/alter code
3. Turn off Client side validation
4. Chapter Questions (Chs 11,12)


#### README.md file should include the following items:

* Screenshots of site

 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


####    Assignment Screenshots:

#### 1.Screenshot of Petstore :

![My Portfolio](img/a5.png)


#### 2.Screenshot of Petstore :
![My Portfolio](img/a5_2.png)






