> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Nicholas Buddemeyer

### Lis4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "A1 README")
    * Install AMPPS
    * Install JDK
    * Install Android Studio
	* Provide Screenshots of install
	* Create Bitbucket Repo




2. [A2 README.md](a2/README.md "A2 README")
	* Create Healthy Recipes Android app
	* Provide screenshots of completed app





3. [A3 README.md](a3/README.md "A3 README")
	* Create ERD based upon business rules
	* Provide screenshot of completed ERD
	* Provide DB resource links


4. [P1 README.md](p1/README.md "P1 README")
	* Create a launcher icon image and display it in both activities (screens)
	* Must add background color(s) to both activities
	* Must add border around image and button
	* Must add text shadow (button)
	* Provide screenshots of completed app

5. [A4 README.md](a4/README.md "A4 README")
	* Review subdirectories and files
	* Open index.php and review code
	* Provide Screenshots of site
	
6. [A5 README.md](a5/README.md "A5 README")
	* Review subdirectories and files
	* Open index.php and review/alter code
	* Turn off Client side validation
	* Provide Screenshots of site 

7. [P2 README.md](p2/README.md "P2 README")
	* Review subdirectories and files
	* Open index.php and review/alter code
	* Turn off Client side validation
	* Add edit and delete files

	

