> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS4381 - Mobile Web Application Development 

## Nicholas Buddemeyer

### Assignment 3 Requirements:

*Three Parts:*

1. Database based upon business rules
2. Bitbucket link
3. Chapter Questions (Chs 5,6)
4. Bitbucket repo link with assignment files

#### README.md file should include the following items:

* Screenshots of ERD
* Links to associated files

 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


####    Assignment Screenshots:

#### 1.Screenshot of ERD:

![ERD Screenshot](img/a3.png)

*Screenshot of running java Hello*:

#### 2.MBW File:
[Pet Store MWB](mysql/a3.mwb)

#### 3.SQL File:
[Pet Store SQL](mysql/a3.sql)

