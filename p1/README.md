> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS4381 - Mobile Web Application Development 

## Nicholas Buddemeyer

### Project 1 Requirements:

*Three Parts:*

1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)


#### README.md file should include the following items:

* Screenshots of Application User Interface


 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


####    Assignment Screenshots:

#### 1.Screenshot of First User Interface:

![User Interface Screenshot](img/p1.png)

#### 2.Screenshot of Second User Interface:

![User Interface Screenshot](img/p1_2.png)




